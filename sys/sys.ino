/*
 * Include the required libraries 
 */
#include <Adafruit_GPS.h>
#include <Adafruit_BMP085.h>
#include <SparkFunLSM9DS1.h>
#include <Servo.h>
#include <SPI.h>
#include <SD.h>
#include <Wire.h>

/*
 * Definitions 
 */
#define MYSERIAL Serial1
#define DEBUG true
#define SERIALPRINT false
#define SDWRITE true
#define LATCHPIN 2
#define WRITEFREQ 50    // 50 ms = 20Hz
#define CONTROLFREQ 250 // 250 ms = 4Hz
#define DAILYPRESSURE 101450
#define SERVOREADPIN A3   // Analog Pin used to read servo input

/*
 * For Servo
 */
#define NEUTRAL_DURATION 20000
#define LEFT_DURATION 20000
#define RIGHT_DURATION 20000
#define SPIN_DURATION 2000
#define LEFT 90
#define RIGHT 96
#define NEUTRAL 93

/*
 * For IMU 
 */
LSM9DS1         imu;
#define LSM9DS1_M   0x1E  // Would be 0x1C if SDO_M is LOW
#define LSM9DS1_AG  0x6B  // Would be 0x6A if SDO_AG is LOW
#define PRINT_CALCULATED
#define DECLINATION -8.58 // Declination (degrees) in Boulder, CO.

/*
 * Instantiate Sensors 
 */
Adafruit_GPS    GPS(&MYSERIAL);
Adafruit_BMP085 bmp;
Servo myServo; 

/*
 * Chip select pin for SD Logger 
 */
const int       chipSelect = 10;

/*
 * Accelerometer Pin Setup 
 */
const int       xpin = A0;
const int       ypin = A1;
const int       zpin = A2;

/*
 * LED Pins 
 */
const int       led_gps = 2;
const int       led_bar = 3;
const int       led_sd = 4;
const int       led_imu = 5;
const int       led_fix = 6;

/*
 * Create timer 
 */
uint32_t        timer = millis();
uint32_t        stateTimer = millis();
uint32_t        servoTimer = millis();

int state = 0;  // 0: Neutral, 1: left, 2:right
int servoState = 0;  // 0: Still, 1: Moving

/*
 * Setup code 
 */
void setup()
{
    /*
     * Setup Serial 
     */
    Serial.begin(115200);

    /*
     * Setup digital pins 
     */
    pinMode(led_gps, OUTPUT);
    pinMode(led_bar, OUTPUT);
    pinMode(led_sd, OUTPUT);
    pinMode(led_imu, OUTPUT);
    pinMode(led_fix, OUTPUT);

    /*
     * Flash them at the beginning 
     */
    flashLeds();

    /*
     * Setup sensors 
     */
    setupGPS();
    // setupBarometer();
    setupSD();
    setupIMU();
    setupServo();

    // delay(20000); // Delay before 

    writeToDataFile("NEW TEST - Skip baro");
}

/*
 * Loop code 
 */
void loop()
{
    /* 
     *  Servo
     *  
     */
    servoStateCheck();
    
    /*
     * GPS loop 
     */
    if (GPS.newNMEAreceived())  // Checks for NMEA
    {
		if (!GPS.parse(GPS.lastNMEA())) {
		  return;   // return if NMEA is not well-formed
		}
    }

    /*
     * Wrap around timer 
     */
    if (timer > millis()) {
  		timer = millis();
    }

    /*
     * check gps fix 
     */
    if (GPS.fix == 1) {
		digitalWrite(led_fix, HIGH);
    } else if (GPS.fix == 0) {
		digitalWrite(led_fix, LOW);
    }

    /*
     * Write data 
     */
    if (millis() - timer >= WRITEFREQ) {
		timer = millis(); // Updates the timer
		String dataString = getDataString();

	  	/*
	   	* PRINT and SAVE 
	   	*/
	  	if (SDWRITE) {
	  		writeToDataFile(dataString);
	  	}
		if (SERIALPRINT) {
			Serial.println(dataString);		
	    }
	}
}

/*
 * Setup GPS 
 */
void setupGPS()
{
    Serial.print("GPS setup...");

    /*
     * GPS Setup code 
     */
    GPS.begin(9600);
    MYSERIAL.begin(9600);

    /*
     * Configure GPS 
     */
    GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
    GPS.sendCommand(PMTK_SET_NMEA_UPDATE_10HZ);
    GPS.sendCommand(PGCMD_ANTENNA);

    /*
     * Add some delay to be safe 
     */
    delay(1000);
    MYSERIAL.println(PMTK_Q_RELEASE);

    Serial.println("Completed");
    digitalWrite(led_gps, HIGH);
}

/*
 * Setup Data Logger 
 */
void setupSD()
{
	if (SDWRITE) {
		Serial.print("SD Card Logger setup...");

    /*
    * Add some delay to be safe 
    */
    delay(1000);

	  	/*
	   	* Check for microSD card 
	   	*/
	  	if (!SD.begin(chipSelect)) {
		    Serial.println("Failed - microSD card not found");
		    while (DEBUG);
	  	}

		

		Serial.println("Completed");
		digitalWrite(led_sd, HIGH);
	} else {
		Serial.println("SDWRITE false");
	}
}

/*
 * Setup Barometer 
 */
void setupBarometer()
{
    Serial.print("Barometer setup...");
    
    delay(1000);
    
    if (!bmp.begin()) {

  		Serial.println("Failed");
  		//while (DEBUG);
    } else {
  		
 		Serial.println("Completed");
  		digitalWrite(led_bar, HIGH);
    }
}

/*
 * Setup IMU 
 */
void setupIMU()
{
    Serial.print("IMU setup...");

    imu.settings.device.commInterface = IMU_MODE_I2C;
    imu.settings.device.mAddress = LSM9DS1_M;
    imu.settings.device.agAddress = LSM9DS1_AG;
    // The above lines will only take effect AFTER calling
    // imu.begin(), which verifies communication with the IMU
    // and turns it on.
    if (!imu.begin()) {
		Serial.println("Failed");
		while (DEBUG);
    } else {
		delay(1000);
		Serial.println("Completed");
		digitalWrite(led_imu, HIGH);
    }
}

void setupServo(){
  myServo.attach(8); 
  myServo.write(93);
}
/*
 * Writes a string to the SD card 
 */
void writeToDataFile(String dataString)
{
    /*
     * Open the data file on the SD card 
     */
    File dataFile = SD.open("datalog.txt", FILE_WRITE);

    /*
     * If the file is available, write to it 
     */
    if (dataFile) {
		dataFile.println(dataString);
		dataFile.close();
    }
    /*
     * If the file failed to open, throw an error 
     */
    else {
		Serial.println("Error opening datalog.txt");
    }
}

/*
 * Creates the data string to be writted 
 */
String getDataString()
{
    /*
     * Update the IMU 
     */
    imu.readGyro();
    imu.readAccel();
    imu.readMag();


    /*
     * Create dataString 
     */
    String dataString = "";
    dataString += millis(); 									dataString += ",";
    dataString += (GPS.fix, DEC); 								dataString += ",";
    dataString += (GPS.latitude, 4); 							dataString += ",";
    dataString += (GPS.longitude, 4); 							dataString += ",";
    dataString += (String(bmp.readAltitude(DAILYPRESSURE), 4)); dataString += ",";
    dataString += (String(imu.calcGyro(imu.gx), 2)); 			dataString += ",";
    dataString += (String(imu.calcGyro(imu.gy), 2)); 			dataString += ",";
    dataString += (String(imu.calcGyro(imu.gz), 2)); 			dataString += ",";
    dataString += (String(imu.calcAccel(imu.ax), 2)); 			dataString += ",";
    dataString += (String(imu.calcAccel(imu.ay), 2)); 			dataString += ",";
    dataString += (String(imu.calcAccel(imu.az), 2)); 			dataString += ",";
    dataString += (String(imu.calcMag(imu.mx), 2)); 			dataString += ",";
    dataString += (String(imu.calcMag(imu.my), 2)); 			dataString += ",";
    dataString += (String(imu.calcMag(imu.mz), 2)); 			dataString += ",";
    dataString += analogRead(SERVOREADPIN);
    return dataString;
}

/*
 * Flash LEDS 
 */
void flashLeds()
{
    digitalWrite(led_gps, HIGH);
    digitalWrite(led_bar, HIGH);
    digitalWrite(led_sd, HIGH);
    digitalWrite(led_imu, HIGH);
    digitalWrite(led_fix, HIGH);
    delay(500);
    digitalWrite(led_gps, LOW);
    digitalWrite(led_bar, LOW);
    digitalWrite(led_sd, LOW);
    digitalWrite(led_imu, LOW);
    digitalWrite(led_fix, LOW);
    delay(500);
}

/*
 * Controller output
 */
 double getControllerDegreeChange(double lat, double lon)    // current latitude and longitude
 {

     // CONSTANTS - WILL BE MOVED OUTSIDE OF FUNCTION WHEN COMPLETE
     double Kp;   // Proportional Gain
     double Ki;   // Integral Gain
     double Kd;   // Derivative Gain

     double lat_d;    // Desired latitude
     double lon_d;    // Desired longitude

     // Instantaneos data - REQUIRED FOR CALCULATIONS
     double lat_p;    // Last latitude reading
     double lon_p;    // Last longitude reading

     double lat_dv = lat_d - lat;   // Endpoint of desired velocity vector
     double lon_dv = lon_d - lon;   // Endpoint of desired velocity vector

     double lat_v = lat - lat_p;    // Endpoint of system velocity vector
     double lon_v = lon - lon_p;    // Endpoint of system velocity vector

     // Calculate required change in angle of the system
     

     return 1.0;
 }

 void servoStateCheck()
 {
  if (servoState == 1) {
    if (millis() - servoTimer >= SPIN_DURATION) {
      Serial.println("Stopped");
      myServo.write(NEUTRAL);
      servoState = 0;
    }
  }
  
  switch (state) {
    case(0) : {
      if (millis() - stateTimer >= NEUTRAL_DURATION) {
        stateTimer = millis();
        state = 1;
        Serial.print("Switch to left - Moving ...");
        myServo.write(LEFT);
        servoTimer = millis() - SPIN_DURATION/2;
        servoState = 1;
      }
      break;
    }
    case(1) : {
      if (millis() - stateTimer >= LEFT_DURATION) {
        stateTimer = millis();
        state = 2;
        Serial.print("Switch to right - Moving ...");
        myServo.write(RIGHT);
        servoTimer = millis();
        servoState = 1;
      }
      break;
    }
    case(2) : {
      if (millis() - stateTimer >= RIGHT_DURATION) {
        stateTimer = millis();
        state = 0;
        Serial.print("Switch to neutral - Moving ...");
        servoTimer = millis() - SPIN_DURATION/2;
        servoState = 1;
      }
      break;
    }
    default : {
      break;
    }
  }
 }

  /*
   * Writes a value to the servo
   */
  void ServoWrite(int angle){
     myServo.write(30);
  }

